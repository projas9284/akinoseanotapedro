Estas son las entencias para ejecutarse en código SQL:

CREATE DATABASE akisoneanota;

CREATE TABLE meetings(id serial NOT NULL PRIMARY KEY, date date, start_hour integer NOT NULL CHECK(start_hour < 24 and start_hour > -1), start_minutes integer NOT NULL CHECK(start_minutes < 60 and start_minutes > -1), subject varchar(40) NOT NULL, place varchar(40) NOT NULL);

CREATE TABLE workers(id serial NOT NULL PRIMARY KEY, name varchar(30) NOT NULL, lastname varchar(30) NOT NULL, email varchar(30) NOT NULL, cellphone varchar(15) NOT NULL);

CREATE TABLE participants(id serial NOT NULL PRIMARY KEY, meeting_id integer NOT NULL REFERENCES meetings(id), worker_id integer NOT NULL REFERENCES workers(id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE topics(id serial NOT NULL PRIMARY KEY, description varchar(300) NOT NULL, durantion_minutes integer NOT NULL, meeting_id integer NOT NULL REFERENCES meetings(id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE agreements(id serial NOT NULL PRIMARY KEY, description varchar(300) NOT NULL, topic_id integer NOT NULL REFERENCES topics(id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE tasks(id serial NOT NULL PRIMARY KEY, description varchar(300) NOT NULL, completion_date date NOT NULL, status boolean NOT NULL, topic_id integer NOT NULL REFERENCES topics(id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE responsibilities(id serial NOT NULL PRIMARY KEY, task_id integer NOT NULL REFERENCES tasks(id), worker_id integer NOT NULL REFERENCES workers(id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE);

