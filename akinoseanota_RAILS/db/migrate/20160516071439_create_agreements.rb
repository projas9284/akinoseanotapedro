class CreateAgreements < ActiveRecord::Migration
  def change
    create_table :agreements do |t|
      t.string :description
      t.references :Topic, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
