class CreateMeetings < ActiveRecord::Migration
  def change
    create_table :meetings do |t|
      t.date :date
      t.fixnum :start_hour
      t.fixnum :start_minutes
      t.string :subject
      t.string :place

      t.timestamps null: false
    end
  end
end
