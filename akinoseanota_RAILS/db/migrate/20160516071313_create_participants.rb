class CreateParticipants < ActiveRecord::Migration
  def change
    create_table :participants do |t|
      t.references :Meeting, index: true, foreign_key: true
      t.references :Worker, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
