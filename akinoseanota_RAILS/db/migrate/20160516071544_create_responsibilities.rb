class CreateResponsibilities < ActiveRecord::Migration
  def change
    create_table :responsibilities do |t|
      t.references :Task, index: true, foreign_key: true
      t.references :Worker, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
