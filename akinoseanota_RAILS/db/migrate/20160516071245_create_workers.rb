class CreateWorkers < ActiveRecord::Migration
  def change
    create_table :workers do |t|
      t.string :name
      t.string :lastname
      t.string :email
      t.string :cellphone

      t.timestamps null: false
    end
  end
end
