class Responsibility < ActiveRecord::Base
  belongs_to :Task
  belongs_to :Worker
  validates :Task, presence: true
  validates :Worker, presence: true
end
