class Task < ActiveRecord::Base
  belongs_to :Topic
  has_many :responsibilities
  has_many :workers, :through => :responsibilities
  validates :Topic, presence: true
  validates :description, presence: true
  validates :completion_date, presence: true
  validates :status, presence: true
end
