class Meeting < ActiveRecord::Base
	has_many :participants
	has_many :workers, :through => :participants
	has_many :topics
	validates :date, presence: true
	validates :start_hour, presence: true
	validates :start_hour, :numericality => { :greater_than => -1,  :less_than => 24 }
	validates :start_minutes, presence: true
	validates :start_minutes, :numericality => { :greater_than => -1,  :less_than => 60 }
	validates :subject, presence: true
	validates :place, presence: true
end