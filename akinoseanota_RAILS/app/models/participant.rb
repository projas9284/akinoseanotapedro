class Participant < ActiveRecord::Base
  belongs_to :Meeting
  belongs_to :Worker
  validates :Meeting, presence: true
  validates :Worker, presence: true
end
