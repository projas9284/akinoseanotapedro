class Topic < ActiveRecord::Base
  belongs_to :Meeting
  has_many :agreements
  has_many :tasks
  validates :Meeting, presence: true
  validates :description, presence: true
  validates :duration_minutes, presence: true
end
