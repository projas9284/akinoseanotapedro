class Worker < ActiveRecord::Base
	has_many :participants
	has_many :meetings, :through => :participants
	has_many :responsibilities
	has_many :tasks, :through => :responsibilities
	validates :name, presence: true
	validates :lastname, presence: true
	validates :email, presence: true
	validates :cellphone, presence: true
end
