class Agreement < ActiveRecord::Base
  belongs_to :Topic
  validates :Topic, presence: true
  validates :description, presence: true
end
